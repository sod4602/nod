Node operators
----------------

Distance between nodes :class:`nod.op.distanceBetween()`
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Builds a distanceBetween node and outputs the distance value based on two nodes provided.

.. code-block:: python

	op.distanceBetween(nodeA, nodeB) >> joint.offset


.. note::

	:class:`nod.op` operators use Maya built-in nodes only and do NOT require additional plug-ins



