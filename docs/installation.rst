Installation
=========================

How to install nod framework for Autodesk® Maya


Via Download
--------------

`Download latest zip archive <https://gitlab.com/martintomoya/nod/-/archive/master/nod-master.zip>`_

Via Git
----------

`Clone nod repository <https://gitlab.com/martintomoya/nod.git>`_

| SSH: ``git@gitlab.com:martintomoya/nod.git``



Python Package
---------------

Nod should be installed like any other python package, depending on the local workflow.

**Nod** package will be found under ``/nod/lib/py/`` in the downloaded archive/cloned repository.

Copy the **package** to your default maya scripts folder.

========================== ===========================================================================
Operating System           Path
========================== ===========================================================================
Linux                        ``~/maya/<version>/scripts``
-------------------------- ---------------------------------------------------------------------------
Mac OS                       ``~/Library/Preferences/Autodesk/maya<version>/scripts``
-------------------------- ---------------------------------------------------------------------------
Windows XP                   ``\Documents and Settings\$USER\My Documents\maya\<version>\scripts``
-------------------------- ---------------------------------------------------------------------------
Windows 7 | Vista            ``\Users\$USER\Documents\maya\<version>\scripts``
========================== ===========================================================================


.. note::

	``nod`` framework does NOT contain nor require any plug-ins


Imports
---------

.. code-block:: python

    from nod import Nod
    from nod import op
    import nod.cmds as nc