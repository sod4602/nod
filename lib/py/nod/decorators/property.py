import functools

class DelayedProperty(object):
    '''
    Used for lazy evaluation of an object attribute
    The property should represent non-mutable data, as it replaces itself.
    '''
    def __init__(self, fget):
        self.fget = fget

        # Copy function's docstring and other attributes
        functools.update_wrapper(self, fget)

    def __get__(self, obj, cls):
        if obj is None:
            return self

        value = self.fget(obj)
        setattr(obj, self.fget.__name__, value)
        return value


class DelayedPropertyLegacy(object):

    def __init__(self, func):
        self._func = func
        self.__name__ = func.__name__
        self.__doc__ = func.__doc__

    def __get__(self, obj, klass=None):
        if obj is None:
            return None

        result = obj.__dict__[self.__name__] = self._func(obj)
        return result
