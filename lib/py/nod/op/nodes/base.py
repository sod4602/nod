from nod.cmds import createNode
from nod.channels.base import Suffix


class NodUtilityOperations(object):

    def __init__(self):
        pass

    def distanceBetween(self, node1, node2):
        """
        Builds distanceBetween node and returns an output channel for further connections

        Args:
            node1 (nod.nodetypes.base.NodBase): Start node to measure between
            node2 (nod.nodetypes.base.NodBase): End node to measure between

        Returns:
            (NodChannel)
        """
        nodeUtility = createNode('distanceBetween', n='{}_to_{}_{}'.format(node1, node2, Suffix.DISTANCE), ss=True)

        for i, node in enumerate((node1, node2)):
            node.attr('worldMatrix[0]') >> nodeUtility.attr('inMatrix{}'.format(i+1))
            node.attr('rotatePivotTranslate') >> nodeUtility.attr('point{}'.format(i+1))

        return nodeUtility.attr('distance')


