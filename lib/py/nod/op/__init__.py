from nod.op.nodes.base import NodUtilityOperations
from nod.channels.base import NodChannelUtilityOperations


def average():
    """
    Not Implemented.
    """
    raise NotImplementedError


def clamp(channel, min=None, max=None, name=None):
    """
    Clamp given channel value via clamp node

    Args:
        channel (nod.channels.base.NodChannel): Input Channel
        min (value or nod.channels.base.NodChannel)
        max (value or nod.channels.base.NodChannel)
        name (str): Utility node name. If not specified creates the name based on channel names

    Returns:
        nod.channels.base.NodChannel
    """
    op = NodChannelUtilityOperations(channel)
    return op.clamp(min=min, max=max, name=name)

def blend(channel, other, driver=None, name=None):
    """
    Blend given channels via blendTwoAttr node

    Args:
        channel (nod.channels.base.NodChannel): Input Channel
        other (nod.channels.base.NodChannel): Input Channel
        driver (nod.channels.base.NodChannel): Driver Channel
        name (str): Utility node name. If not specified creates the name based on channel names

    Returns:
        nod.channels.base.NodChannel
    """
    op = NodChannelUtilityOperations(channel)
    return op.blend(other, driver=driver, name=name)

def remap(channel, inputRange=None, outputRange=None, positions=None, values=None, interpolations=None, name=None):
    """
    Remap given channel value via remapValue node

    Args:
        channel (nod.channels.base.NodChannel): Input Channel
        inputRange (tuple): Tuple of Min and Max input values (or NodChannel)
        outputRange (tuple): Tuple of Min and Max output values (or NodChannel)
        positions (list): List of positions for each value provided. Ignored if no values
        values (list): List of values for each position. Ignored if no positions
        interpolations (list)): List of interpolation modes for each position. If None, all use default Linear mode
                                    0 - None
                                    1 - Linear (default)
                                    2 - Smooth
                                    3 - Spline
        name (str): Utility node name. If not specified creates the name based on channel names

    Returns:
        nod.channels.base.NodChannel
    """
    if all([positions, values]):
        if not len(positions) == len(values):
            raise ValueError('Positions not matching values: {} : {}'.format(positions, values))
    else:
        positions, values = None, None

    op = NodChannelUtilityOperations(channel)
    return op.remap(inputRange=inputRange, outputRange=outputRange,
                    positions=positions, values=values, interpolations=interpolations, name=name)

def abs(channel):
    """
    Creates an abs. Currently this has to be done via two power node power1/2(power2)

    Args:
        channel (nod.channels.base.NodChannel): Input Channel

    Returns:
        nod.channels.base.NodChannel
    """
    return (channel ** 2) ** 0.5

def reverse(channel, name=None):
    """
    Reverse given channel value via reverse node

    Args:
        channel (nod.channels.base.NodChannel): Input Channel
        name (str): Utility node name. If not specified creates the name based on channel names

    Returns:
        nod.channels.base.NodChannel
    """
    op = NodChannelUtilityOperations(channel)
    return op.reverse(name=name)


def condition(firstTerm, operator, secondTerm, ifTrue=None, ifFalse=None, name=None):
    """
    Condition operation where inputs and outputs can be either constant values or other channels


    Args:
        firstTerm (nod.channels.base.NodChannel): A driving channel
        operator (str): Compare operator:
                        '==', '!=', '>', '>=', '<', '<='
                        Alternatively operation name can be provided following maya description:
                        'equal', 'not equal', 'greater than', 'greater or equal', 'less than', 'less or equal'

        secondTerm (NodChannel, float): A driving channel or value
        ifTrue (NodChannel, float or tuple): A resulting channel or value(s) if condition is True
        ifFalse (NodChannel, float or tuple): A resulting channel or value(s) if condition is False
        name (str): Utility node name. If not specified creates the name based on channel names

    Returns:
        nod.channels.base.NodChannel
    """
    operatorDict = {'==': 0, 'equal': 0,
                    '!=': 1, 'not equal': 1,
                    '>': 2, 'greater than': 2,
                    '>=': 3, 'greater or equal': 3,
                    '<': 4, 'less than': 4,
                    '<=': 5, 'less or equal': 5}

    operationMode = operatorDict.get(operator.lower(), None)
    if operationMode is None:
        raise ValueError('Incorrect condition operator: "{}" Valid operators:\n{}'.format(operator,
                                                                                    '\n'.join(operatorDict.keys())))

    op = NodChannelUtilityOperations(firstTerm)
    return op.condition(secondTerm, operationMode=operationMode, ifTrue=ifTrue, ifFalse=ifFalse, name=name)


def distanceBetween(node1, node2):
    """
    Builds distanceBetween node and returns an output channel for further connections

    Args:
        node1 (nod.nodetypes.base.NodBase): Start node to measure between
        node2 (nod.nodetypes.base.NodBase): End node to measure between

    Returns:
        nod.channels.base.NodChannel
    """
    op = NodUtilityOperations()
    return op.distanceBetween(node1, node2)
