from nod.op.nodes.base import NodUtilityOperations


def distanceBetween(node1, node2):
    """
    Measures distance between node and returns the float value

    Args:
        node1 (nod.nodetypes.base.NodBase): Start node to measure between
        node2 (nod.nodetypes.base.NodBase): End node to measure between

    Returns:
        float: Distance value
    """
    op = NodUtilityOperations()
    channel = op.distanceBetween(node1, node2)
    distanceValue = channel.get()

    channel.nod.delete()
    return distanceValue
