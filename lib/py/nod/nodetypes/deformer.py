import maya.api.OpenMaya as om
import maya.cmds as mc

from nod.decorators.property import DelayedProperty
from nod.nodetypes.base import NodBase
from nod.channels.base import NodChannel
from nod.channels.callables import NodChannelCallableOutputs


class NodDeformer(NodBase):

    DEFORMER_TYPE = ''

    def __init__(self, *args):
        super(NodDeformer, self).__init__(*args)

    @DelayedProperty
    def envelope(self):
        """
        Deformer envelope channel

        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'envelope')

    @ DelayedProperty
    def outputGeometry(self):
        """
        This returns a callable NodChannel property.
        When called returns output geometry list for the current deformer node
        Otherwise serves as a regular NodChannel

        Returns:
            list: List of nod.nodetypes.transform.NodTransform
        """
        return NodChannelCallableOutputs(self, 'outputGeometry', parents=True)

    @DelayedProperty
    def dependNode(self):
        """
        Returns api2.0 MObject

        Returns:
            maya.api.OpenMaya.MObject
        """
        return self._mobject

    @DelayedProperty
    def _mobject(self):
        return om.MSelectionList().add(self.name()).getDependNode(0)
