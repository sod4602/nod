import maya.cmds as mc

from nod.decorators.property import DelayedProperty

from nod.nodetypes.transform import NodTransform
from nod.channels.base import NodChannel


class NodIkHandle(NodTransform):

    def __init__(self, *args):
        super(NodIkHandle, self).__init__(*args)

    def effector(self):
        """
        Returns:
            NodEffector: Effector node
        """
        return NodEffector(mc.ikHandle(self.name(), ee=True, q=True))

    @DelayedProperty
    def offset(self):
        return NodChannel(self, 'offset')

    @DelayedProperty
    def poleVector(self):
        return NodChannel(self, 'poleVector')

    @DelayedProperty
    def poleVectorX(self):
        return NodChannel(self, 'poleVectorX')

    @DelayedProperty
    def poleVectorY(self):
        return NodChannel(self, 'poleVectorY')

    @DelayedProperty
    def poleVectorZ(self):
        return NodChannel(self, 'poleVectorZ')

    @DelayedProperty
    def roll(self):
        return NodChannel(self, 'roll')

    @DelayedProperty
    def twist(self):
        return NodChannel(self, 'twist')

    @DelayedProperty
    def ikBlend(self):
        return NodChannel(self, 'ikBlend')

    @DelayedProperty
    def startJoint(self):
        """
        Start joint of the given handle
        """
        return self.attr('startJoint').input().node()

    @DelayedProperty
    def endJoint(self):
        """
        End joint of the given handle
        """
        return self.effector.attr('tx').input().node()

    @DelayedProperty
    def effector(self):
        """
        End effector of the given handle
        """
        return self.attr('endEffector').input().node()

    @DelayedProperty
    def curve(self):
        """
        If connected returns in curve transform node for instance IK Spline

        Returns:
            nod.nodetypes.transform.NodTransform: In Curve transform node
        """
        input = self.attr('inCurve').input()
        if not input:
            return

        return input.node().parent()

    @DelayedProperty
    def twistControls(self):
        """
        Get the TwistControlsHandle object to access advanced twist controls

        Returns:
            _TwistControlsHandle
        """
        return _TwistControlsHandle(self)

    @DelayedProperty
    def enums(self):
        """

        Returns:
            Enums
        """
        return Enums()

    def solvedJoints(self):
        """
        Returns joints that are solved by the given handle

        Returns:
            list: List of joints
        """
        joints = [self.endJoint]
        startJoint = self.startJoint

        while joints[-1]:

            parent = joints[-1].parent()
            if not parent:
                break

            joints.append(parent)
            if str(parent) == str(startJoint):
                break

        joints.reverse()
        return joints


class NodEffector(NodTransform):

    def __init__(self, *args):
        super(NodEffector, self).__init__(*args)


class _TwistControlsHandle(object):

    def __init__(self, ikh):
        """
        Twist Controls attributes Handle

        Args:
            ikh (NodIkHandle): IkHandle object to attach the twist attributes controls on
        """
        self.ikh = ikh

    def enable(self):
        self.ikh.attr('dTwistControlEnable').set(True)

    def disable(self):
        self.ikh.attr('dTwistControlEnable').set(False)

    @DelayedProperty
    def worldUpType(self):
        return self.ikh.attr('dWorldUpType')

    @DelayedProperty
    def forwardAxis(self):
        return self.ikh.attr('dForwardAxis')

    @DelayedProperty
    def worldUpAxis(self):
        return self.ikh.attr('dWorldUpAxis')

    @DelayedProperty
    def worldUpVector(self):
        return self.ikh.attr('dWorldUpVector')

    @DelayedProperty
    def worldUpVectorEnd(self):
        return self.ikh.attr('dWorldUpVectorEnd')

    @DelayedProperty
    def worldUpMatrix(self):
        return self.ikh.attr('dWorldUpMatrix')

    @DelayedProperty
    def worldUpMatrixEnd(self):
        return self.ikh.attr('dWorldUpMatrixEnd')

    @DelayedProperty
    def twistValueType(self):
        return self.ikh.attr('dTwistValueType')


class Enums(object):

    WUT_SCENE_UP = 0
    WUT_OBJECT_UP = 1
    WUT_OBJECT_UP_START_END = 2
    WUT_OBJECT_ROTATION_UP = 3
    WUT_OBJECT_ROTATION_UP_START_END = 4
    WUT_VECTOR = 5
    WUT_VECTOR_START_END = 6
    WUT_RELATIVE = 7

    FORWARD_AXIS_X = 0
    FORWARD_AXIS_X_NEG = 1
    FORWARD_AXIS_Y = 2
    FORWARD_AXIS_Y_NEG = 3
    FORWARD_AXIS_Z = 4
    FORWARD_AXIS_Z_NEG = 5

    UP_AXIS_Y = 0
    UP_AXIS_Y_NEG = 1
    UP_AXIS_CLOSEST_Y = 2
    UP_AXIS_Z = 3
    UP_AXIS_Z_NEG = 4
    UP_AXIS_CLOSEST_Z = 5
    UP_AXIS_X = 6
    UP_AXIS_X_NEG = 7
    UP_AXIS_CLOSEST_X = 8

    TWIST_TYPE_TOTAL = 0
    TWIST_TYPE_START_END = 1
    TWIST_TYPE_RAMP = 2
