import maya.cmds as mc
import maya.api.OpenMaya as om

from nod.nodetypes.dag import NodDag


class NodShape(NodDag):

    def __init__(self, *args):
        super(NodShape, self).__init__(*args)