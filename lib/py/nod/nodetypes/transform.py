import maya.cmds as mc

from nod.decorators.property import DelayedProperty

from nod.channels.base import NodChannel

from nod.nodetypes.dag import NodDag
from nod.nodetypes.base import Nod
from nod.decorators import cast


class NodTransform(NodDag):

    def __init__(self, *args):
        super(NodTransform, self).__init__(*args)

    @DelayedProperty
    def t(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 't')

    @DelayedProperty
    def r(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'r')

    @DelayedProperty
    def s(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 's')

    @DelayedProperty
    def translate(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'translate')

    @DelayedProperty
    def rotate(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'rotate')

    @DelayedProperty
    def scale(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'scale')

    @DelayedProperty
    def tx(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'tx')

    @DelayedProperty
    def ty(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'ty')

    @DelayedProperty
    def tz(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'tz')

    @DelayedProperty
    def rx(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'rx')

    @DelayedProperty
    def ry(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'ry')

    @DelayedProperty
    def rz(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'rz')

    @DelayedProperty
    def sx(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'sx')

    @DelayedProperty
    def sy(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'sy')

    @DelayedProperty
    def sz(self):
        """
        Returns:
            nod.channels.base.NodChannel
        """
        return NodChannel(self, 'sz')

    def lockTransforms(self, hide=False):
        self.t.lock()
        self.r.lock()
        self.s.lock()

        if not hide:
            return

        self.t.hide()
        self.r.hide()
        self.s.hide()

    def unlockTransforms(self):
        for attr in self._transformAttrs():
            getattr(self, attr).unlock()

    def limitTranslate(self, x=None, y=None, z=None):
        raise NotImplementedError

    def limitRotate(self, x=None, y=None, z=None):
        raise NotImplementedError

    def limitScale(self, x=None, y=None, z=None):
        raise NotImplementedError

    def matrix(self, exclusive=False, inverse=False):
        """
        Returns the matrix for all transforms in the path, including the node

        Args:
            exclusive (bool): If True get matrix for all transforms in the path, excluding the current node
            inverse (bool): If True return inverse matrix

        Returns:
            om.MMatrix: Matrix
        """
        if exclusive:
            if inverse:
                return self.dagPath.exclusiveMatrixInverse()
            return self.dagPath.exclusiveMatrix()

        if inverse:
            return self.dagPath.inclusiveMatrixInverse()
        return self.dagPath.inclusiveMatrix()

    def objectCenter(self, *args, **kwargs):
        """
        Utilizing maya.cmds.objectCenter() function
        """
        return mc.objectCenter(str(self), *args, **kwargs)

    def centerPivots(self):
        """
        Center nodes pivots
        """
        mc.xform(str(self), cp=True)

    def snapTo(self, other, position=False, orient=False, pivot=False):
        """
        Snaps the transforms of the current node to another given nod

        # TODO: Extract logic to separate module

        Args:
            other (Nod, str): Either another Nod or a string name
            position (bool): If True match the node position
            orient (bool): If True match the node orientation
            pivot (bool): If True match the node rotate and scalePivots, only useful when not position snapping
        """
        name = self.name()
        other = Nod(other)
        nameOther = str(other)

        if position:
            if '[' in nameOther:
                #TODO: Use programmatic way once NodComponent is implemented. Currently this is the fastest check
                mc.xform(name, ws=True, t=mc.xform(nameOther, q=True, ws=True, t=True))

            mc.delete(mc.pointConstraint(nameOther, self.name(), mo=False))
        elif pivot:
            mc.xform(name, ws=True, rp=mc.xform(nameOther, q=True, ws=True, rp=True))
            mc.xform(name, ws=True, sp=mc.xform(nameOther, q=True, ws=True, sp=True))

        if orient:
            mc.delete(mc.orientConstraint(nameOther, self.name(), mo=False))

    def snapPivotTo(self, other):
        """
        Snaps pivot to given node
        """
        _copyPivot(other, self)

    def freeze(self, t=True, r=True, s=True):
        """
        Freeze node transforms

        Args:
            t (bool): If True freeze translate channels
            r (bool): If True freeze rotate channels
            s (bool): If True freeze scale channels
        """
        mc.makeIdentity(self.name(), apply=True, t=t, r=r, s=s)

    def move(self, x, y, z, **kwargs):
        """
        Using maya.cmds.move arguments. Operates on the current node

        Args:
            x (float): X value
            y (float): Y value
            z (float): Z value
        """
        mc.move(x, y, z, self.name(), **kwargs)

    def xform(self, **kwargs):
        """
        Using maya.cmds.xform on self
        """
        return mc.xform(self.name(), **kwargs)

    @staticmethod
    def _transformAttrs():
        attrs = ['{}{}'.format(trs, xyz) for trs in 'trs' for xyz in 'xyz']
        attrs.extend('trs')
        return attrs


@cast.nodArgumentsAsStrings
def _copyPivot(nodFrom, nodTo):
    """
    Copies pivot from one node to another 
    """
    if '.' in nodFrom:
        # Fast check if it's a component
        mc.xform(nodTo, ws=True, rp=mc.xform(nodFrom, q=True, ws=True, t=True))
        mc.xform(nodTo, ws=True, sp=mc.xform(nodFrom, q=True, ws=True, t=True))
        return

    mc.xform(nodTo, ws=True, rp=mc.xform(nodFrom, q=True, ws=True, rp=True))
    mc.xform(nodTo, ws=True, sp=mc.xform(nodFrom, q=True, ws=True, sp=True))
