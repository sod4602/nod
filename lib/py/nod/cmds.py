import maya.cmds as mc
from nod.decorators import cast

def _fi(i):
    if isinstance(i, list):
        return i[0]
    return i

def selected():
    """
    List selected nodes as Nod objects

    Returns:
        (iterator): Iterates through all selected Nod(s)
    """
    uuids = mc.ls(sl=True, uuid=True)
    if not uuids:
        return

    from nod.nodetypes.base import NodByUuid
    for uuid in uuids:
        yield NodByUuid(uuid)

def selectedAt(index):
    """
    A single node is returned rather than generator
    This is for the user convenience of grabbing a single node at given index.
    e.x.: last or first selected node (common scenario)

    Args:
        index (int): Index of the selected node

    Returns:
        (nod.nodetypes.base.NodBase): Selected Nod of index
    """
    uuids = mc.ls(sl=True, uuid=True)
    if not uuids:
        return

    from nod.nodetypes.base import NodByUuid
    return NodByUuid(uuids[index])

"""
Maya cmds overridden section
"""

def addAttr(*args, **kwargs):
    """
    Not Implemented. Use Nod.addAttr() instead
    """
    raise NotImplementedError('Use Nod.addAttr() method instead.')

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def aimConstraint(*args, **kwargs):
    """
    Maya aim constraint

    Returns:
        nod.nodetypes.base.Nod
    """
    return _fi(mc.aimConstraint(*args, **kwargs))

def aliasAttr(*args, **kwargs):
    """
    Not Implemented. Use NodChannel.addAlias() instead
    """
    raise NotImplementedError('User NodChannel.addAlias() instead')

@cast.nodArgumentsAsStrings
def arclen(*args, **kwargs):
    return mc.arclen(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def attachCurve(*args, **kwargs):
    return mc.attachCurve(*args, **kwargs)

@cast.nodArgumentsAsStrings
def attributeInfo(*args, **kwargs):
    return mc.attributeInfo(*args, **kwargs)

@cast.nodArgumentsAsStrings
def attributeQuery(*args, **kwargs):
    return mc.attributeQuery(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def bindSkin(*args, **kwargs):
    return mc.bindSkin(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def blendShape(*args, **kwargs):
    return mc.blendShape(*args, **kwargs)[0]

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def blendTwoAttr(*args, **kwargs):
    return mc.blendTwoAttr(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def boneLattice(*args, **kwargs):
    return mc.boneLattice(*args, **kwargs)

@cast.returnStringsAsNods
def camera(*args, **kwargs):
    """
    Returns camera transform node rather than transform, shape tuple

    Returns:
        nod.nodetypes.transform.NodTransform
    """
    return mc.camera(*args, **kwargs)[0]

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def circle(*args, **kwargs):
    return mc.circle(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def cluster(*args, **kwargs):
    return mc.cluster(*args, **kwargs)

def condition(*args, **kwargs):
    """
    Not Implemented. Use nod.op.condition() operator instead
    """
    raise NotImplementedError('Use nod.op.condition() operator method instead.')

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def cone(*args, **kwargs):
    return mc.cone(*args, **kwargs)

def connectAttr(*args, **kwargs):
    """
    Not Implemented. Use NodChannel >> NodChannel syntax instead
    """
    raise NotImplementedError('Use "NodChannel >> NodChannel" instead.')

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def connectionInfo(*args, **kwargs):
    return mc.connectionInfo(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def connectJoint(*args, **kwargs):
    return mc.connectJoint(*args, **kwargs)

@cast.nodArgumentsAsStrings
def constructionHistory(*args, **kwargs):
    return mc.constructionHistory(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def createNode(*args, **kwargs):
    """
    Returns:
        Nod.nodetypes.base.NodBase
    """
    return mc.createNode(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def closeCurve(*args, **kwargs):
    return mc.closeCurve(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def curve(*args, **kwargs):
    return mc.curve(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def curveOnSurface(*args, **kwargs):
    return mc.curveOnSurface(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def cylinder(*args, **kwargs):
    return mc.cylinder(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def deformer(*args, **kwargs):
    return mc.deformer(*args, **kwargs)

@cast.nodArgumentsAsStrings
def delete(*args, **kwargs):
    return mc.delete(*args, **kwargs)

def deleteAttr(*args, **kwargs):
    """
    Not Implemented. Use NodChannel.delete() instead
    """
    raise NotImplementedError('Use NodChannel.delete() instead')

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def deltaMush(*args, **kwargs):
    return mc.deltaMush(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def detachCurve(*args, **kwargs):
    return mc.detachCurve(*args, **kwargs)

def disconnectAttr(*args, **kwargs):
    """
    Not Implemented. Use NodChannel.disconnect() instead
    """
    raise NotImplementedError('Use NodChannel.disconnect() instead')

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def distanceDimension(*args, **kwargs):
    return mc.distanceDimension(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def duplicate(*args, **kwargs):
    return mc.duplicate(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def duplicateSurface(*args, **kwargs):
    return mc.duplicateSurface(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def effector(*args, **kwargs):
    """
    Returns:
        Nod.nodetypes.ikhandle.NodEffector
    """
    return mc.effector(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def findType(*args, **kwargs):
    return mc.findType(*args, **kwargs)

def getAttr(*args, **kwargs):
    """
    Not Implemented. Use NodChannel.get() instead
    """
    raise NotImplementedError('Use NodChannel.get() instead')

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def group(*args, **kwargs):
    """
    Maya group command

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.base.Nod
    """
    return mc.group(*args, **kwargs)

def hide(*args, **kwargs):
    """
    Not Implemented. Use Nod.hide() instead.
    """
    raise NotImplementedError('Use Nod.hide() method instead.')

@cast.nodArgumentsAsStrings
def inheritTransform(*args, **kwargs):
    return mc.inheritTransform(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def insertJoint(*args, **kwargs):
    return mc.insertJoint(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def ikHandle(*args, **kwargs):
    """
    Returns ikHandle only. Effector can be queried using NodIkHandle.effector() method

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.ikhandle.NodIkHandle
    """
    return mc.ikHandle(*args, **kwargs)[0]

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def ikSolver(*args, **kwargs):
    """
    Maya ikSolver command

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.base.Nod
    """
    return mc.ikSolver(*args, **kwargs)

@cast.nodArgumentsAsStrings
def isDirty(*args, **kwargs):
    return mc.isDirty(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def joint(*args, **kwargs):
    """
    Maya joint command

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.joint.NodJoint
    """
    return mc.joint(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def jointCluster(*args, **kwargs):
    """
    Maya jointCluster command

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.base.Nod
    """
    return mc.jointCluster(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def jointLattice(*args, **kwargs):
    """
    Maya jointLattice command

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.base.Nod
    """
    return mc.jointLattice(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def lattice(*args, **kwargs):
    """
    Maya lattice command

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.base.Nod
    """
    return mc.lattice(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def listAttr(*args, **kwargs):
    """
    Maya listAttr command

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.base.Nod
    """
    return mc.listAttr(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def listConnections(*args, **kwargs):
    """
    Maya ListConnections command.
    NodChannel is loaded with additional commands like input() / outputs() etc.

    Args:
        *args: 
        **kwargs: 

    Returns:
        list
    """
    return mc.listConnections(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def listHistory(*args, **kwargs):
    return mc.listHistory(*args, **kwargs)

def lockNode(*args, **kwargs):
    """
    Not Implemented. Use Nod.lock() instead
    """
    raise NotImplementedError('User Nod.lock() instead')

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def loft(*args, **kwargs):
    return mc.loft(*args, **kwargs)[0]

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def ls(*args, **kwargs):
    return mc.ls(*args, **kwargs)

@cast.nodArgumentsAsStrings
def makeIdentity(*args, **kwargs):
    return mc.makeIdentity(*args, **kwargs)

def mirrorJoint(*args, **kwargs):
    """
    Not Implemented. Use NodJoint.mirror() instead'
    """
    raise NotImplementedError('Use NodJoint.mirror() method instead.')

@cast.nodArgumentsAsStrings
def move(*args, **kwargs):
    return mc.move(*args, **kwargs)

@cast.nodArgumentsAsStrings
def nodeCast(*args, **kwargs):
    return mc.nodeCast(*args, **kwargs)

@cast.nodArgumentsAsStrings
def nodeType(*args, **kwargs):
    return mc.nodeType(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def nonLinear(*args, **kwargs):
    return mc.nonLinear(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def normalConstraint(*args, **kwargs):
    """
    Maya normalConstraint

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.base.Nod
    """
    return _fi(mc.normalConstraint(*args, **kwargs))

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def nurbsCube(*args, **kwargs):
    return mc.nurbsCube(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def nurbsPlane(*args, **kwargs):
    return mc.nurbsPlane(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def nurbsSquare(*args, **kwargs):
    return mc.nurbsSquare(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def nurbsToPoly(*args, **kwargs):
    return mc.nurbsToPoly(*args, **kwargs)

def objectCenter(*args, **kwargs):
    """
    Not Implemented. Use NTransform.objectCenter() method instead.
    """
    raise NotImplementedError('Use NTransform.objectCenter() method instead.')

@cast.nodArgumentsAsStrings
def objectType(*args, **kwargs):
    return mc.objectType(*args, **kwargs)

@cast.nodArgumentsAsStrings
def objExists(*args, **kwargs):
    return mc.objExists(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def orientConstraint(*args, **kwargs):
    """
    Maya orientConstraint

    Returns:
        nod.nodetypes.constraint.NodOrientConstraint
    """
    return _fi(mc.orientConstraint(*args, **kwargs))

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def parent(*args, **kwargs):
    return mc.parent(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def parentConstraint(*args, **kwargs):
    """
    Maya orientConstraint

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.constraint.NodParentConstraint
    """
    return _fi(mc.parentConstraint(*args, **kwargs))

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def pickWalk(*args, **kwargs):
    return mc.pickWalk(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def plane(*args, **kwargs):
    return mc.plane(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def pointConstraint(*args, **kwargs):
    """
    Maya pointConstraint

    Returns:
        nod.nodetypes.constraint.NodPointConstraint
    """
    return _fi(mc.pointConstraint(*args, **kwargs))

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def pointCurveConstraint(*args, **kwargs):
    return _fi(mc.pointCurveConstraint(*args, **kwargs))

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def pointOnPolyConstraint(*args, **kwargs):
    """
    Maya pointOnPolyConstraint

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.base.Nod
    """
    return _fi(mc.pointOnPolyConstraint(*args, **kwargs))

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def pointOnCurve(*args, **kwargs):
    return mc.pointOnCurve(*args, **kwargs)

@cast.nodArgumentsAsStrings
def pointPosition(*args, **kwargs):
    return mc.pointPosition(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def poleVectorConstraint(*args, **kwargs):
    """
    Maya poleVectorConstraint

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.constraint.NodPoleVectorConstraint
    """
    return _fi(mc.poleVectorConstraint(*args, **kwargs))

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyCone(*args, **kwargs):
    return mc.polyCone(*args, **kwargs)

# @cast.nodArgumentsAsStrings
# @cast.returnStringsAsNods
# def polyContourProjection(*args, **kwargs):
#   return mc.polyContourProjection(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyCube(*args, **kwargs):
    return mc.polyCube(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyCylinder(*args, **kwargs):
    return mc.polyCylinder(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyPlane(*args, **kwargs):
    return mc.polyPlane(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyPlatonicSolid(*args, **kwargs):
    return mc.polyPlatonicSolid(*args, **kwargs)

# @cast.nodArgumentsAsStrings
# @cast.returnStringsAsNods
# def polyProjectCurve(*args, **kwargs):
#   return mc.polyProjectCurve(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyPyramid(*args, **kwargs):
    return mc.polyPyramid(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyQuad(*args, **kwargs):
    return mc.polyQuad(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polySmooth(*args, **kwargs):
    return mc.polySmooth(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polySphere(*args, **kwargs):
    return mc.polySphere(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyToCurve(*args, **kwargs):
    return mc.polyToCurve(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyTorus(*args, **kwargs):
    return mc.polyTorus(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def polyTransfer(*args, **kwargs):
    return mc.polyTransfer(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def rebuildCurve(*args, **kwargs):
    return mc.rebuildCurve(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def rebuildSurface(*args, **kwargs):
    return mc.rebuildSurface(*args, **kwargs)[0]

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def removeJoint(*args, **kwargs):
    return mc.removeJoint(*args, **kwargs)

def rename(*args, **kwargs):
    """
    Not implemented. Use Nod.rename() instead
    """
    raise NotImplementedError('Use Nod.rename() instead.')

def renameAttr():
    """
    Not implemented. Use NodChannel.rename() instead
    """
    raise NotImplementedError('Use NodChannel.rename() instead')

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def reroot(*args, **kwargs):
    return mc.reroot(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def reverseCurve(*args, **kwargs):
    return mc.reverseCurve(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def reverseSurface(*args, **kwargs):
    return mc.reverseSurface(*args, **kwargs)

@cast.nodArgumentsAsStrings
def rotate(*args, **kwargs):
    return mc.rotate(*args, **kwargs)

@cast.nodArgumentsAsStrings
def scale(*args, **kwargs):
    return mc.scale(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def scaleConstraint(*args, **kwargs):
    """
    Maya scaleConstraint

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.constraint.NodScaleConstraint
    """
    return _fi(mc.scaleConstraint(*args, **kwargs))

@cast.nodArgumentsAsStrings
def select(*args, **kwargs):
    return mc.select(*args, **kwargs)

@cast.nodArgumentsAsStrings
def setAttr(*args, **kwargs):
    return mc.setAttr(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def skinCluster(*args, **kwargs):
    return mc.skinCluster(*args, **kwargs)

@cast.nodArgumentsAsStrings
def skinPercent(*args, **kwargs):
    return mc.skinPercent(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def smoothCurve(*args, **kwargs):
    return mc.smoothCurve(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def spaceLocator(*args, **kwargs):
    """
    SpaceLocator.

    Returns locator transform node rather than transform, shape tuple

    Args:
        *args: 
        **kwargs: 

    Returns:
        nod.nodetypes.transform.NodTransform
    """
    return mc.spaceLocator(*args, **kwargs)[0]

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def sphere(*args, **kwargs):
    return mc.sphere(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def substituteGeometry(*args, **kwargs):
    return mc.substituteGeometry(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def surface(*args, **kwargs):
    return mc.surface(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def text(*args, **kwargs):
    return mc.text(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def torus(*args, **kwargs):
    return mc.torus(*args, **kwargs)

@cast.nodArgumentsAsStrings
def transformLimits(*args, **kwargs):
    """
    In addition to transformLimits command check:
    Nod.limit[Translate | Rotate | Scale]() methods')
    """
    return mc.transformLimits(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def transferAttributes(*args, **kwargs):
    return mc.transferAttributes(*args, **kwargs)

@cast.nodArgumentsAsStrings
def ungroup(*args, **kwargs):
    return mc.ungroup(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def wire(*args, **kwargs):
    return mc.wire(*args, **kwargs)

@cast.nodArgumentsAsStrings
def xform(*args, **kwargs):
    return mc.xform(*args, **kwargs)

@cast.nodArgumentsAsStrings
@cast.returnStringsAsNods
def xformConstraint(*args, **kwargs):
    return _fi(mc.xformConstraint(*args, **kwargs))
