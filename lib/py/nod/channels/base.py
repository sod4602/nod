import re
import inspect

import maya.cmds as mc

from nod.cmds import createNode
from maya.api import OpenMaya as om

from nod.decorators import cast

class Suffix(object):

    ADD = 'add'
    AVERAGE = 'avg'
    BLEND = 'blend'
    CLAMP = 'clamp'
    CONDITION = 'cond'
    DISTANCE = 'dist'
    DIVIDE = 'div'
    MULTIPLY = 'mul'
    POWER = 'pow'
    REMAPVALUE = 'rmpv'
    REVERSE = 'rev'
    SUBTRACT = 'sub'



class NodChannelUtilityOperations(object):

    MULTIPLY = 'multiply'
    DIVIDE = 'divide'
    POWER = 'power'
    ADD = 'plus'
    SUBTRACT = 'minus'
    AVERAGE = 'average'

    def __init__(self, channel):
        """
        Args:
            channel (nod.channels.base.NodChannel): Channel to do operations on

        Returns:
            (nod.channels.base.NodChannel): Resulting utility node output channel
        """
        self.channel = channel

    def multiply(self, other, name=None):
        """
        Multiply the channel with a value(s) or another channel

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: MultiplyDivide utility node output channel for further connections
        """
        return self._multiplyDivide(other, operation=self.MULTIPLY, name=name)

    def _rmultiply(self, other, name=None):
        """
        Multiply the channel with a value(s) or another channel (right hand)

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: MultiplyDivide utility node output channel for further connections
        """
        return self._multiplyDivide(other, operation=self.MULTIPLY, rightHand=True, name=name)

    def divide(self, other, name=None):
        """
        Divide the channel with a value(s) or another channel

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: MultiplyDivide utility node output channel for further connections
        """
        return self._multiplyDivide(other, operation=self.DIVIDE, name=name)

    def _rdivide(self, other, name=None):
        """
        Divide the channel with a value(s) or another channel (right hand)

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: MultiplyDivide utility node output channel for further connections
        """
        return self._multiplyDivide(other, operation=self.DIVIDE, rightHand=True, name=name)

    def power(self, other, name=None):
        """
        Power the channel with a value(s) or another channel

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: MultiplyDivide utility node output channel for further connections
        """
        return self._multiplyDivide(other, operation=self.POWER, name=name)

    def _rpower(self, other, name=None):
        """
        Power the channel with a value(s) or another channel (right hand)

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: MultiplyDivide utility node output channel for further connections
        """
        return self._multiplyDivide(other, operation=self.POWER, rightHand=True, name=name)

    def _multiplyDivide(self, other, operation=None, rightHand=False, name=None):
        """
        TODO: Make this a class

        Operations on the multiplyDivide node

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            operation (str): Multiply, Divide, Power
            rightHand (bool): For right hand side operations (__rmul__, __rdiv__, __rpow__)
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: MultiplyDivide utility node output channel for further connections
        """
        operation = operation.lower()
        mode = dict(multiply=1, divide=2, power=3)
        suffix = dict(multiply=Suffix.MULTIPLY, divide=Suffix.DIVIDE, power=Suffix.POWER)

        name = name or self.__utilityNodeName(suffix.get(operation, 'none'))
        utilityNode = createNode('multiplyDivide', n=name, ss=True)
        utilityNode.attr('operation').set(mode.get(operation, 0))

        return self._multiplyDivideConnections(other, utilityNode, rightHand=rightHand)

    def add(self, other, name=None):
        """
        Add the channel with a value(s) or another channel

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: PlusMinusAverage utility node output channel for further connections
        """
        return self._plusMinusAverage(other, operation=self.ADD, name=name)

    def _radd(self, other, name=None):
        """
        Add the value(s) or another channel to the current channel (right hand)

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: PlusMinusAverage utility node output channel for further connections
        """
        return self._plusMinusAverage(other, operation=self.ADD, rightHand=True, name=name)

    def subtract(self, other, name=None):
        """
        Subtract the channel with a value(s) or another channel

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: PlusMinusAverage utility node output channel for further connections
        """
        return self._plusMinusAverage(other, operation=self.SUBTRACT, name=name)

    def _rsubtract(self, other, name=None):
        """
        Subtract the channel with a value(s) or another channel (right hand)

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: PlusMinusAverage utility node output channel for further connections
        """
        return self._plusMinusAverage(other, operation=self.SUBTRACT, rightHand=True, name=name)

    def average(self, other, name=None):
        """
        Average the channel with a value(s) or another channel

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: PlusMinusAverage utility node output channel for further connections
        """
        return self._plusMinusAverage(other, operation=self.AVERAGE, name=name)

    def _plusMinusAverage(self, other, operation=None, rightHand=False, name=None):
        """
        TODO: Make this a class

        Operations on the plusMinusAverage node

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            operation (str): Plus, Minus, Average
            rightHand (bool): For right hand side operations (__radd__, __rsub__)
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: PlusMinusAverage utility node output channel for further connections
        """
        operation = operation.lower()
        mode = dict(plus=1, minus=2, average=3)
        suffix = dict(plus=Suffix.ADD, minus=Suffix.SUBTRACT, average=Suffix.AVERAGE)

        name = name or self.__utilityNodeName(suffix.get(operation, 'none'))
        utilityNode = createNode('plusMinusAverage', n=name, ss=True)
        utilityNode.attr('operation').set(mode.get(operation, 0))

        return self._plusMinusAverageConnections(other, utilityNode, rightHand=rightHand)

    def _plusMinusAverageConnections(self, other, utilityNode, rightHand=False):
        """
        TODO: Support multiple other channels. Currently single channel at one time

        Internal logic for plusMinusAverage connections

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            utilityNode (nod.nodetypes.base.NodBase): Utility node (plusMinusAverage)
            rightHand (bool): For right hand side operations (__radd__)

        Returns:
            nod.channels.base.NodChannel: Utility node output channel for further connections
        """
        # In order of probability for best performance
        depth = self.channel.depth()
        slotA, slotB = self.__plusMinusConnectionSlotIndexes(rightHand)

        if depth == 1:
            self.channel >> utilityNode.attr('input1D[{}]'.format(slotA))

            if self.isChannel(other):
                other >> utilityNode.attr('input1D[{}]'.format(slotB))

            elif self.isSingleValue(other):
                utilityNode.attr('input1D[{}]'.format(slotB)).set(other)

            elif self.isMultiValue(other):
                raise ValueError('Invalid value {} for attribute {}'.format(other, self.channel))

            return utilityNode.attr('output1D')

        elif depth == 3:
            self.channel >> utilityNode.attr('input3D[{}]'.format(slotA))

            if self.isChannel(other):
                other >> utilityNode.attr('input3D[{}]'.format(slotB))

            elif self.isSingleValue(other):
                for xyz in 'xyz':
                    utilityNode.attr('input3D[{}].input3D{}'.format(slotB, xyz)).set(other)

            elif self.isMultiValue(other):
                if len(other) != 3:
                    raise ValueError('Invalid value {} for attribute {}'.format(other, self.channel))

                utilityNode.attr('input3D[{}]'.format(slotB)).set(other)

            return utilityNode.attr('output3D')

        elif depth == 2:
            self.channel >> utilityNode.attr('input2D[{}]'.format(slotA))

            if self.isChannel(other):
                other >> utilityNode.attr('input2D[{}]'.format(slotB))

            elif self.isSingleValue(other):
                for xy, value in zip('xy', list(other)):
                    utilityNode.attr('input2D[{}].input2D{}'.format(slotB, xy)).set(value)

            elif self.isMultiValue(other):
                if len(other) != 2:
                    raise ValueError('Invalid value {} for attribute {}'.format(other, self.channel))

                utilityNode.attr('input2D[{}]'.format(slotB)).set(other)

            return utilityNode.attr('output2D')

    def _multiplyDivideConnections(self, other, utilityNode, rightHand=False):
        """
        Internal logic for multiplyDivide nodes connections

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            utilityNode (nod.nodetypes.base.NodBase): Utility node (multiplyDivide)
            rightHand (bool): For right hand side operations (__rmul__, __rdiv__, __rpow__)

        Returns:
            nod.channels.base.NodChannel: Utility node output channel for further connections
        """
        channelXYZ = self._whichChannelXYZ(self.channel)
        slotA, slotB = self.__multiplyDivideConnectionSlotIndexes(rightHand)

        # In order of probability for best performance
        depth = self.channel.depth()
        if depth == 1:
            self.channel >> utilityNode.attr('input{}{}'.format(slotA, channelXYZ))

            if self.isChannel(other):
                other >> utilityNode.attr('input{}{}'.format(slotB, channelXYZ))

            elif self.isSingleValue(other):
                utilityNode.attr('input{}{}'.format(slotB, channelXYZ)).set(other)

            elif self.isMultiValue(other):
                raise ValueError('Invalid value {} for attribute {}'.format(other, self.channel))

            return utilityNode.attr('output{}'.format(channelXYZ))

        elif depth == 3:
            self.channel >> utilityNode.attr('input{}'.format(slotA))

            if self.isChannel(other):
                other >> utilityNode.attr('input{}'.format(slotB))

            elif self.isSingleValue(other):
                for xyz in 'XYZ':
                    utilityNode.attr('input{}{}'.format(slotB, xyz)).set(other)

            elif self.isMultiValue(other):
                if len(other) != 3:
                    raise ValueError('Invalid value {} for attribute {}'.format(other, self.channel))

                utilityNode.attr('input{}'.format(slotB)).set(other)

            return utilityNode.attr('output')

        elif depth == 2:
            self.channel >> utilityNode.attr('input{}'.format(slotA))

            if self.isChannel(other):
                other >> utilityNode.attr('input{}'.format(slotB))

            elif self.isSingleValue(other):
                for xy, value in zip('XY', list(other)):
                    utilityNode.attr('input{}{}'.format(slotB, xy)).set(value)

            elif self.isMultiValue(other):
                if len(other) != 2:
                    raise ValueError('Invalid value {} for attribute {}'.format(other, self.channel))

                utilityNode.attr('input{}'.format(slotB)).set(other)

            return utilityNode.attr('output')

    def blend(self, other, driver=None, name=None):
        """
        Blending two attributes via blendTwoAttr node

        Args:
            other (nod.channels.base.NodChannel/float/list): Channel or values to operate with
            driver (nod.channels.base.NodChannel): Channel that serves as the blend driver (0-1)

        Returns:
            nod.channels.base.NodChannel: Utility node output channel for further connections
        """
        name = name or self.__utilityNodeName(Suffix.BLEND)
        blendNode = createNode('blendTwoAttr', n=name, ss=True)

        depth = self.channel.depth()
        if not depth == 1:
            raise NotImplementedError('Only single channel attributes are supported i.e.: tx OR ty OR tz')

        driver >> blendNode.attr('attributesBlender')
        self.channel >> blendNode.attr('input[0]')

        if self.isChannel(other):
            other >> blendNode.attr('input[1]')

        elif self.isSingleValue(other):
            blendNode.attr('input[1]').set(other)

        elif self.isMultiValue(other):
            raise ValueError('Invalid value {} for blending attribute {}'.format(other, self.channel))

        return blendNode.attr('output')

    def clamp(self, min=None, max=None, name=None):
        """
        TODO: Make this a class
        Operations on the clamp node

        Args:
            min (nod.channels.base.NodChannel/float/tuple): Minimum value(s) either single (r), tuple(r, b), or tuple(r, g, b)
            max (nod.channels.base.NodChannel/float/tuple): Maximum value(s) either single (r), tuple(r, b), or tuple(r, g, b)
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: Clamp utility node output channel for further connections
        """
        name = name or self.__utilityNodeName(Suffix.CLAMP)
        clampNode = createNode('clamp', n=name, ss=True)
        channelRGB = self._whichChannelRGB(self.channel)

        # In order of probability for best performance
        depth = self.channel.depth()
        if depth == 1:
            self.channel >> clampNode.attr('input{}'.format(channelRGB))

            for minmax, attributeName in zip([min, max], ['min', 'max']):

                if self.isChannel(minmax):
                    minmax >> clampNode.attr('{}{}'.format(attributeName, channelRGB))

                elif self.isSingleValue(minmax):
                    clampNode.attr('{}{}'.format(attributeName, channelRGB)).set(minmax)

                elif self.isMultiValue(minmax):
                    raise ValueError('Invalid {} value {} for channel {}'.format(attributeName, minmax, self.channel))

            return clampNode.attr('output{}'.format(channelRGB))

        elif depth == 3:
            self.channel >> clampNode.attr('input')

            for minmax, attributeName in zip([min, max], ['min', 'max']):

                if self.isChannel(minmax):
                    self.channel  >> clampNode.attr('input')

                elif self.isSingleValue(minmax):
                    for rgb in 'RGB':
                        clampNode.attr('{}{}'.format(attributeName, rgb)).set(minmax)

                elif self.isMultiValue(minmax):
                    if len(minmax) != 3:
                        raise ValueError(
                            'Invalid {} value {} for channel {}'.format(attributeName, minmax, self.channel))

                    clampNode.attr(attributeName).set(minmax)

            return clampNode.attr('output')

        elif depth == 2:
            self.channel >> clampNode.attr('input')

            for minmax, attributeName in zip([min, max], ['min', 'max']):

                if self.isChannel(minmax):
                    self.channel >> clampNode.attr('input')

                elif self.isSingleValue(minmax):
                    for rg in 'RG':
                        clampNode.attr('{}{}'.format(attributeName, rg)).set(minmax)

                elif self.isMultiValue(minmax):
                    if len(minmax) != 2:
                        raise ValueError(
                            'Invalid {} value {} for channel {}'.format(attributeName, minmax, self.channel))

                    clampNode.attr(attributeName).set(minmax)

            return clampNode.attr('output')

    def condition(self, secondTerm, operationMode, ifTrue=None, ifFalse=None, name=None):
        """
        TODO: Make this a class
        Operations on the condition node

        Args:
            secondTerm (nod.channels.base.NodChannel/float): A driving channel or value
            operationMode (int): Compare operation mode following maya attributes
                                0: 'equal'
                                1: 'not equal'
                                2: 'greater than'
                                3: 'greater or equal'
                                4: 'less than'
                                5: 'less or equal'
            ifTrue (nod.channels.base.NodChannel/float/tuple): A resulting channel or value(s) if condition is True
            ifFalse (nod.channels.base.NodChannel/float/tuple): A resulting channel or value(s) if condition is False
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: Condition node output channel
        """
        depth = self.channel.depth()
        if not depth == 1:
            raise ValueError('Invalid channel depth {} for channel {}'.format(depth, self.channel))

        name = name or self.__utilityNodeName(Suffix.CONDITION)
        conditionNode = createNode('condition', n=name, ss=True)
        conditionNode.attr('operation').set(operationMode)
        self.channel >> conditionNode.attr('firstTerm')

        if self.isChannel(secondTerm):
            secondTerm >> conditionNode.attr('secondTerm')

        elif self.isSingleValue(secondTerm):
            conditionNode.attr('secondTerm').set(secondTerm)

        else:
            raise ValueError('Invalid value {} for condition secondTerm'.format(secondTerm))

        channelRGB = None
        for trueFalse, channelOrValue in zip(('True', 'False'), (ifTrue, ifFalse)):
            channelRGB = self._setOrConnectConditionBool(conditionNode, channelOrValue, trueFalse,
                                                         channelRGB=channelRGB)

        if channelRGB:
            return conditionNode.attr('outColor{}'.format(channelRGB))

        return conditionNode.attr('outColor')

    def _setOrConnectConditionBool(self, conditionNode,  channelOrValue, trueFalse, channelRGB=None):

        attributeName = 'colorIf{0}'.format(trueFalse.title())

        if self.isChannel(channelOrValue):
            # Use the same channel extension for both true and false inputs
            depth = channelOrValue.depth()

            if depth == 1:
                if not channelRGB:
                    channelRGB = self._whichChannelRGB(channelOrValue)

                channelOrValue >> conditionNode.attr('{}{}'.format(attributeName, channelRGB))

            elif depth == 3:
                channelOrValue >> conditionNode.attr(attributeName)

            else:
                # TODO: Can we support this
                raise ValueError('Connection not recognized: {} for condition {}'.format(channelOrValue, trueFalse))

        elif self.isSingleValue(channelOrValue):
            channelRGB = 'R'
            conditionNode.attr('{}{}'.format(attributeName, channelRGB)).set(channelOrValue)

        elif self.isMultiValue(channelOrValue):
            conditionNode.attr(attributeName).set(channelOrValue)

        else:
            raise ValueError('Invalid value {} for channel {}'.format(channelOrValue, attributeName))

        return channelRGB

    def remap(self, inputRange=None, outputRange=None, positions=None, values=None, interpolations=None, name=None):
        """


        Args:
            channel (nod.channels.base.NodChannel): Input Channel
            inputRange (nod.channels.base.NodChannel/tuple): Tuple of Min and Max input values (or NodChannel)
            outputRange (nod.channels.base.NodChannel/tuple): Tuple of Min and Max output values (or NodChannel)
            positions (list): List of positions for each value provided. Ignored if no values
            values (list): List of values for each position. Ignored if no positions
            interpolations (list)): List of interpolation modes for each position. If None, all use default Linear mode
                                    0 - None
                                    1 - Linear (default)
                                    2 - Smooth
                                    3 - Spline
            name (str): Utility node name. If not specified creates the name based on channel names

        Returns:
            nod.channels.base.NodChannel: Remap node output channel
        """
        depth = self.channel.depth()
        if not depth == 1:
            raise ValueError('Invalid channel depth {} for channel {}'.format(depth, self.channel))

        name = name or self.__utilityNodeName(Suffix.REMAPVALUE)
        remapNode = createNode('remapValue', n=name, ss=True)
        channelRGB = self._whichChannelRGB(self.channel)

        self.channel >> remapNode.attr('inputValue')

        for ioMinMax, attributeName in zip(inputRange + outputRange,
                                           ['inputMin', 'inputMax', 'outputMin', 'outputMax']):

            if self.isChannel(ioMinMax):
                ioMinMax >> remapNode.attr(attributeName)

            elif self.isSingleValue(ioMinMax):
                remapNode.attr(attributeName).set(ioMinMax)

            elif self.isMultiValue(ioMinMax):
                raise ValueError('Invalid value {} for channel {}'.format(ioMinMax, attributeName))

        for i, position in enumerate(positions or []):
            remapNode.attr('value[{}].value_Position'.format(i)).set(position)
            remapNode.attr('value[{}].value_FloatValue'.format(i)).set(values[i])
            if interpolations:
                remapNode.attr('value[{}].value_Interp'.format(i)).set(interpolations[i])

        return remapNode.attr('outValue')

    def reverse(self, name=None):
        """
        Returns:
            (NodChannel)
        """
        name = name or self.__utilityNodeName(Suffix.REVERSE)
        reverseNode = createNode('reverse', n=name, ss=True)
        channelXYZ = self._whichChannelXYZ(self.channel)

        depth = self.channel.depth()

        if depth == 1:
            self.channel >> reverseNode.attr('input{}'.format(channelXYZ))
            return reverseNode.attr('output{}'.format(channelXYZ))

        elif depth == 3:
            self.channel >> reverseNode.attr('input')
            return reverseNode.attr('output')

        else:
            raise ValueError('Invalid channel depth {} for channel {}'.format(depth, self.channel))

    @classmethod
    def isSingleValue(cls, other):
        return isinstance(other, (int, float, bool))

    @classmethod
    def isMultiValue(cls, other):
        return isinstance(other, (tuple, list))

    @classmethod
    def isChannel(cls, other):
        return isinstance(other, NodChannel)

    def _whichChannelXYZ(self, channel):
        """
        Given a channel determines which axis XYZ is appropriate for connections.
        To keep connections clean and consistent connections should look like:
                x-x, y-y, z-z
                r-x, g-y, b-z
                (...)
        Args:
            channel (nod.channels.base.NodChannel): Channel to determine the axis from

        Returns:
            (str): XYZ
        """
        pattern = '[a-zA-Z0-9]+([XYZRGB]{1})$'
        matched = re.match(pattern, channel.attribute)
        if not matched:
            return 'X'

        conversion = dict(X='X', Y='Y', Z='Z', R='X', G='Y', B='Z')
        return conversion.get(matched.groups()[0])

    def _whichChannelRGB(self, channel):
        """
        Given a channel determines which channel RGB is appropriate for connections.
        To keep connections clean and consistent connections should look like:
                r-r, g-g, b-b
                x-r, y-g, z-b
                (...)
        Args:
            channel (nod.channels.base.NodChannel): Channel to determine the channel component from

        Returns:
            (str): RGB
        """
        pattern = '[a-zA-Z0-9]+([XYZRGB]{1})$'
        matched = re.match(pattern, channel.attribute)
        if not matched:
            return 'R'

        conversion = dict(R='R', G='G', B='B', X='R', Y='G', Z='B')
        return conversion.get(matched.groups()[0])

    def __utilityNodeName(self, suffix, other=None, name=None):

        def __shortestOther(o):
            if self.isChannel(o):
                return '_{}_{}'.format(other.nod.name(), other.attributeShortName())

            if o is None:
                return ''

            return '_{}'.format(o)

        return '{}_{}{}{}'.format('{}_{}'.format(self.channel.nod.name(), self.channel.attributeShortName()),
                                   __shortestOther(other),
                                   '_{}'.format(name) if name else '',
                                   suffix)

    def __plusMinusConnectionSlotIndexes(self, rightHand):
        slotA = 1 if rightHand else 0
        slotB = 0 if rightHand else 1
        return slotA, slotB

    def __multiplyDivideConnectionSlotIndexes(self, rightHand):
        slotA = 2 if rightHand else 1
        slotB = 1 if rightHand else 2
        return slotA, slotB

class NodChannel(object):

    def __init__(self, nod, attribute):
        self.nod = nod
        self.attribute = attribute
        # Cast in long form for consistency
        self.attribute = self.attributeLongName()

    @classmethod
    def byPlugName(cls, plugName):
        """
        Args:
            plugName (str): Full plug name i.e.: "locator1.translateX"

        Returns:
            nod.channels.base.NodChannel: NodChannel object
        """
        buffer = plugName.split('.')
        if not len(buffer) == 2:
            raise ValueError('{} is not a full plug name i.e.: "locator1.translateX'.format(plugName))

        from nod.nodetypes.base import Nod
        name, attribute = buffer
        return cls(Nod(name), attribute)

    def __repr__(self):
        return '<{} "{}.{}">'.format(self.__class__, self.nod.name(), self.attribute)

    def __str__(self):
        return '{}.{}'.format(self.nod.name(), self.attribute)

    def __contains__(self, channel):
        if not isinstance(channel, NodChannel):
            raise TypeError('"{}" is not a NodChannel object')

        return channel.isConnectedTo(self)

    def __lshift__(self, otherOuts):
        """
        Connect to multiple outputs
        
        i.e.:
            locator.tx << (joint.tx, transform.tx)
            
        Args:
            otherOuts (list/tuple): List/tuple of channels to connect to
        """
        if not isinstance(otherOuts, (list, tuple)):
            raise TypeError('"{}" is not a list/tuple. Provide a list of multiple connections')

        for otherOut in otherOuts:
            self.__shift(self, otherOut)

    def __rshift__(self, otherIn):
        """
        Connect to a single output

        i.e.:
            locator.tx >> joint.tx

        Args:
            otherIn (nod.channels.base.NodChannel): Channel to connect to
        """
        return self.__shift(self, otherIn)

    def __ne__(self, other):
        if self == other:
            mc.disconnectAttr(self, other)
        elif other == self:
            mc.disconnectAttr(other, self)

        print '{} disconnected from {}'.format(self, other)

    def __eq__(self, other):
        """
        Checks if the current channel is identical to the one compared

        Args:
            other (nod.channels.base.NodChannel): Other channel to check

        Returns:
            bool
        """
        if not self.node() == other.node():
            return False

        return self.attribute == other.attribute

    def __add__(self, other):
        """
        Add the channel via utility node with the other channel or value
        
        NOTE: If a string is added to the channel a new NodChannel object is created using the following logic:

            NodChannel.attributeName + '_suffix'
            results in
            NodChannel.attributeName_suffix

        Example
            node.tx + 3 (add 3 to translateX channel)
            node.translate + [1, 2, 3] (add 1, 2, 3 to tx, ty, tz channels respectively)
            node.translate + 3 (add 3 to each translate channel tx, ty and tz)
            node.translate + node2.rotate (Add each xyz translate channel to corresponding xyz rotate channel)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to add the current channel to

        Returns:
            nod.channels.base.NodChannel: Output channel of the plusMinusAverage utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        if isinstance(other, basestring):
            from nod.nodetypes.base import Nod
            return Nod(str(self) + other)

        op = NodChannelUtilityOperations(self)
        return op.add(other)

    def __radd__(self, other):
        """
        Add the channel via utility node with the other channel or value

        Example:
            node.tx + 3 (add 3 to translateX channel)
            node.translate + [1, 2, 3] (add 1, 2, 3 to tx, ty, tz channels respectively)
            node.translate + 3 (add 3 to each translate channel tx, ty and tz)
            node.translate + node2.rotate (Add each xyz translate channel to corresponding xyz rotate channel)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to add the current channel to

        Returns:
            nod.channels.base.NodChannel: Output channel of the plusMinusAverage utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op._radd(other)

    def __sub__(self, other):
        """
        Subtract the channel via utility node from the other channel or value

        Example:
            node.tx - 3 (subtract 3 from translateX channel)
            node.translate - [1, 2, 3] (subtract 1, 2, 3 from tx, ty, tz channels respectively)
            node.translate - 3 (subtract 3 from each translate channel tx, ty and tz)
            node.translate - node2.rotate (subtract each xyz rotate channel from corresponding xyz translate channels)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to subtract the current channel from

        Returns:
            nod.channels.base.NodChannel: Output channel of the plusMinusAverage utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op.subtract(other)

    def __rsub__(self, other):
        """
        Subtract the channel via utility node from the other channel or value

        Example:
            3 - node.tx (subtract translateX channel from constant value 3)
            [1, 2, 3] - node.translate (subtract tx, ty, tz from 1, 2, 3 values respectively)
            3 - node.translate (subtract each translate channel tx, ty and tz from values = 3)
            node.translate - node2.rotate (subtract each xyz rotate channel from corresponding xyz translate channels)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to subtract the current channel from

        Returns:
            nod.channels.base.NodChannel: Output channel of the plusMinusAverage utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op._rsubtract(other)

    def __mul__(self, other):
        """
        Multiply the channel via utility node with the other channel or value

        Example:
            node.tx * 3 (multiply translateX channel by 3)
            node.translate * [1, 2, 3] (multiply tx by 1, ty by 2, tz by 3) Saves having to create multiple nodes.
            node.translate * 3 (multiply each translate channel xyz by 3 )
            node.translate * node2.rotate (multiply each xyz translate channel by corresponding xyz rotate channel)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to multiply the current channel by

        Returns:
            nod.channels.base.NodChannel: Output channel of the multiplyDivide utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op.multiply(other)

    def __rmul__(self, other):
        """
        Multiply the channel via utility node with the other channel or value

        Example:
            3 * node.tx (multiply translateX channel by 3)
            [1, 2, 3] * node.translate (multiply tx by 1, ty by 2, tz by 3) Saves having to create multiple nodes.
            3 * node.translate (multiply each translate channel xyz by 3 )
            node.translate * node2.rotate (multiply each xyz translate channel by corresponding xyz rotate channel)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to multiply the current channel by

        Returns:
            nod.channels.base.NodChannel: Output channel of the multiplyDivide utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op._rmultiply(other)

    def __div__(self, other):
        """
        Divide the channel via utility node with the other channel or value

        Example:
            node.tx / 3 (divide translateX channel by 3)
            node.translate / [1, 2, 3] (divide tx by 1, ty by 2, tz by 3) Saves having to create multiple nodes.
            node.translate / 3 (divide each translate channel xyz by 3 )
            node.translate / node2.rotate (divide each xyz translate channel by corresponding xyz rotate channel)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to divide the current channel by

        Returns:
            nod.channels.base.NodChannel: Output channel of the multiplyDivide utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op.divide(other)

    def __rdiv__(self, other):
        """
        Divide the channel via utility node with the other channel or value

        Example:
            3 / node.tx (divide 3 by translateX channel)
            [1, 2, 3] / node.translate (divide 1 by tx, 2 by ty, 3 by tz) Saves having to create multiple nodes.
            3 / node.translate (divide 3 by each translate channel xyz)
            node.translate / node2.rotate (divide each xyz translate channel by corresponding xyz rotate channel)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to divide the current channel by

        Returns:
            nod.channels.base.NodChannel: Output channel of the multiplyDivide utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op._rdivide(other)

    def __pow__(self, other, modulo=None):
        """
        Power the channel via utility node with the other channel or value

        Example:
            node.tx ** 3 (power translateX channel by 3)
            node.tx ** 0.5 (sqrt translateX channel by 3)
            node.translate ** [1, 2, 3] (power tx by 1, ty by 2, tz by 3) Saves having to create multiple nodes.
            node.translate ** 3 (power each translate channel xyz by 3 )
            node.translate ** node2.rotate (power each xyz translate channel by corresponding xyz rotate channel)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to power the current channel by

        Returns:
            nod.channels.base.NodChannel: Output channel of the multiplyDivide utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op.power(other)

    def __rpow__(self, other, modulo=None):
        """
        Power the channel via utility node with the other channel or value

        Example:
            3 ** node.tx (3 to the power of translateX channel)
            [1, 2, 3] ** node.translate (1 to the power of tx, 2 to ty, 3 to tz) Saves having to create multiple nodes.
            3 ** node.translate (3 to the power of each translate channel xyz)
            node.translate ** node2.rotate (power each xyz translate channel by corresponding xyz rotate channel)

        Args:
            other (nod.channels.base.NodChannel/float): Either another channel or value to power the current channel by

        Returns:
            nod.channels.base.NodChannel: Output channel of the multiplyDivide utility node.
                        The dimension of the output is determined by the dimension of the current channel
        """
        op = NodChannelUtilityOperations(self)
        return op._rpower(other)

    def visualizeOutput(self, attributeName):
        """

        Args:
            attributeName (str):

        Returns:

        """
        raise NotImplementedError

    def attributeShortName(self):
        """
        Current attribute name as attributeShortName

        Returns:
            str: Short attribute name
        """
        try:
            return mc.attributeQuery(self.attribute, node=self.nod.name(), sn=True)
        except RuntimeError:
            return self.attribute

    def attributeLongName(self):
        """
        Current attribute name as attributeLongName

        Returns:
            str: Long attribute name
        """
        for mark in '.[':
            if mark in self.attribute:
                # print 'Multi level attribute provided. Not casting into longName'
                return self.attribute

        return mc.attributeQuery(self.attribute, node=self.nod.name(), ln=True)

    def attributeType(self):
        """
        Returns:
            str: Maya attribute type

            Maya attribute types and example channels:

            addr : addr ambientLightShape1.objectId
            byte : byte AISEnvFacade1.isHistoricallyInteresting
            char : char ambientLightShape1.objectType
            compound : typeFailed aimConstraint1.objectGroups
            double : typeFailed AISEnvFacade1.extraLightShadowWidth
            double2 : TdataCompound texLattice1.latticePoint
            double3 : double3 aimConstraint1.boundingBoxMin
            double4 : double4 aimConstraint1.rotateQuaternion
            doubleAngle : TdataCompound blendDevice1.inputAngle
            doubleLinear : TdataCompound blendDevice1.inputLinear
            enum : enum AISEnvFacade1.nodeState
            float : typeFailed AISEnvFacade1.extraLightIntensity
            float2 : float2 ambientLightShape1.uvCoord
            float3 : float3 AISEnvFacade1.backgroundColor
            floatAngle : floatAngle animBlendNodeAdditiveFA1.inputA
            floatLinear : floatLinear animBlendNodeAdditiveFL1.inputA
            fltMatrix : matrix ambientLightShape1.matrixWorldToEye
            generic : mesh geoConnector1.localGeometry
            lightData : TdataCompound ambientLightShape1.lightData
            long : typeFailed aimConstraint1.objectGroupId
            long2 : TdataCompound subdivToPoly1.outSubdCVId
            long3 : long3 blendShape1.function
            matrix : typeFailed cMuscleCreator1.insertMatrix
            message : message AISEnvFacade1.message
            polyFaces : typeFailed polySurfaceShape1.face
            short : short AISEnvFacade1.antiAliasingQuality
            short2 : short2 greasePlaneShape1.coverage
            time : TdataCompound character1.timeValues
            typed : nurbsCurve bezierCurveToNurbs1.inputCurve
        """
        return mc.attributeQuery(self.attribute, node=self.nod.name(), at=True)

    def isConnectable(self):
        """
        Returns:
            bool: Is the channel connectable
        """
        return mc.attributeQuery(self.attribute, node=self.nod.name(), connectable=True)

    def isConnectedTo(self, other):
        """
        Returns True if both channels are connected

        Args:
            other (nod.channels.base.NodChannel): Other channel to check for connections

        Returns:
            bool
        """
        if other is None:
            raise TypeError('Channel is None')

        return mc.isConnected(str(self), str(other))

    def depth(self):
        """
        Determine the depth of the channel typically [1-3]

        Returns:
            int: Depth of the current channel
        """
        if '.' in self.attribute:
            return 1

        # i.e.: float3, double2, float
        pattern = '[a-zA-Z]+([1-4]{1})$'
        matched = re.match(pattern, self.attributeType())
        # Quick way of checking the depth with regex
        if not matched:
            return 1

        return int(matched.groups()[0])

    def __shift(self, _out, _in):
        """
        Args:
            _out (nod.channels.base.NodChannel): Outgoing channel 
            _in (nod.channels.base.NodChannel): Incoming channel
        """
        if _out == _in:
            print '{} is already connected to {}'.format(_out, _in)
            return

        if _in == _out:
            print 'Reversed connection already exists. Skipping to avoid cycle'
            return

        self.unlock()

        # print 'Connecting "{}" to "{}"'.format(_out, _in)
        mc.connectAttr(str(_out), str(_in), f=True)

    def _lockToggle(self, _bool):
        mc.setAttr(str(self), l=_bool)
        # Additionally toggle the children .i.e.: rotate: rotateX, rotateY, rotateZ
        # Not all attributes can be queried for children and some child attributes might not be lockable
        try:
            for childAttr in mc.attributeQuery(self.attribute, n=self.node().name(), listChildren=True) or []:
                mc.setAttr(self.node().attr(childAttr), l=_bool)
        except RuntimeError:
            return

    def disconnect(self, inputs=True, outputs=True):
        """
        Disconnect channel from other connected channels
        Args:
            inputs (bool): If True disconnect incoming connections
            outputs (bool): If True disconnect outgoing connections

        Returns:
            list: Originally connected channels
        """
        connections = mc.listConnections(str(self), p=True, s=outputs, d=inputs) or []
        for connection in connections:
            self <> NodChannel.byPlugName(connection)

        return connections

    def edit(self, *args, **kwargs):
        """
        Overloading the maya.cmds.addAttr(e=True) mode
        """
        mc.addAttr(str(self), *args, e=True, **kwargs)

    def delete(self):
        """
        Using the maya.cmds.deleteAttr()
        """
        channel = str(self)
        mc.deleteAttr(channel)
        del(self)

    def isLocked(self):
        return mc.getAttr(str(self), lock=True)

    def lock(self):
        self._lockToggle(True)

    def unlock(self):
        self._lockToggle(False)

    def _hideToggle(self, _bool):

        childAttrs = mc.attributeQuery(self.attribute, n=self.node().name(), listChildren=True) or []
        if not childAttrs:
            mc.setAttr(str(self), cb=not _bool)
            mc.setAttr(str(self), k=not _bool)
            return

        # If attr is a parent then toggle the children only .i.e.: rotate: rotateX, rotateY, rotateZ
        for childAttr in childAttrs:
            mc.setAttr(self.node().attr(childAttr), cb=not _bool)
            mc.setAttr(self.node().attr(childAttr), k=not _bool)

    def hide(self):
        self._hideToggle(True)

    def unhide(self):
        self._hideToggle(False)

    def lockAndHide(self):
        self.lock()
        self.hide()

    def unlockAndUnhide(self):
        self.unlock()
        self.unhide()

    def node(self):
        return self.nod

    def parent(self):
        results = mc.attributeQuery(self.attribute, n=self.nod.name(), lp=True)
        if results:
            return self.nod.attr(results[0])

    def rename(self):
        raise NotImplementedError

    def set(self, *args, **kwargs):
        count = len(args)
        if not count:
            mc.setAttr(str(self), **kwargs)
            return

        if count == 1:
            if isinstance(args[0], (tuple, list)):
                mc.setAttr(str(self), *args[0], **kwargs)
                return

        mc.setAttr(str(self), *args, **kwargs)

    def get(self, asVector=False):
        results = mc.getAttr(str(self))

        if asVector==True and isinstance(results, (tuple, list)):
            if len(results[0]) == 3:
                return om.MVector(results[0])

        return results

    def setToDefault(self):
        """
        Sets the attribute value to default value(s)
        If compound attribute is being called all children will be set to default
        """
        raise NotImplementedError

    def setKeyable(self, state):
        """
        Set the value
        Args:
            state (bool): If True attribute is set to keyable
        """
        mc.setAttr(str(self), k=state)

    def setLimits(self, min=None, max=None):
        """
        Sets the attribute value limits
        Args:
            min (float): Minimum value or None (no limit)
            max (float):  Maximum value or None (no limit)
        """
        mc.addAttr(str(self), e=True, hasMinValue=bool(min))
        mc.addAttr(str(self), e=True, hasMaxValue=bool(max))

        if not min is None:
            mc.addAttr(str(self), e=True, minValue=min)

        if not max is None:
            mc.addAttr(str(self), e=True, maxValue=max)

    @cast.returnStringsAsNods
    def connections(self, *args, **kwargs):
        """
        Using maya.cmds.listConnections method

        Returns:
            (list): List of connections
        """
        return mc.listConnections(str(self), *args, **kwargs)

    def input(self, type=None, exactType=False, skipConversionNodes=True):
        """
        Get the incoming connection channel object

        Args:
            type (str): Filter by provided type only i.e.: "joint"
            exactType (bool): When True, type only considers node of this exact type. Otherwise,
                             derived types are also taken into account.
            skipConversionNodes (bool): If True, skip over unit conversion nodes and return the node connected to the
                                conversion node on the other side. Default True.

        Returns:
            nod.channels.base.NodChannel: Input channel
        """
        connections = self.connections(d=False, s=True, plugs=True, type=type or '', exactType=exactType,
                                       scn=skipConversionNodes)
        if connections:
            return connections[0]

    def outputs(self, type=None):
        """
        Get list of outgoing connections channel objects
        
        Returns:
            list: List of output channels
        """
        return self.connections(plugs=True, scn=True, type=type or '') or []


def _asPlugable(item):
    """
    Make an item plugable to be used with an operator.

    Converts instancemethod to a channel if an attribute with the same name exists.
    This allows for methods to overlap with some attribute names as there is never a guarantee that the user
    would not add an attribute to a node shadowing an existing method name.

    This is kept here for reference only and is no longer used.
    """
    if inspect.ismethod(item):
        return __instanceMethodToNodChannel(item)
    return item

def __instanceMethodToNodChannel(instancemethod):
    """
    Given an instancemethod i.e.: Class.Instance.method()
    Find a NodChannel with the same name.

    This is used when a nod object has a

    i.e.:

    <bound method NodBlendShape.outputGeometry of <<class 'nod.nodetypes.blendShape.NodBlendShape'>

    will be converted to:

    <<class 'nod.channels.base.NodChannel'> "blendShape2.outputGeometry">

    Args:
        instancemethod (instancemethod): Instance method on a Nod object

    Returns:
        nod.channels.NodChannel
	"""
    name = instancemethod.im_func.__name__

    try:
        channel = instancemethod.im_self.attr(name)
    except Exception, _e:
        raise TypeError('Can not find channel attribute "{}" on given object'.format(name))
    else:
        return channel
