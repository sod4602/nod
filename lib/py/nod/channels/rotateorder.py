from nod.channels.base import NodChannel


class NodChannelRotateOrder(NodChannel):

    ATTRIBUTE = 'rotateOrder'
    STRING_TO_INT = dict(xyz=0, yzx=1, zxy=2, xzy=3, yxz=4, zyx=5)

    def __init__(self, nod):
        super(NodChannelRotateOrder, self).__init__(nod, self.ATTRIBUTE)

    def set(self, value):
        if isinstance(value, basestring):
            value = self._toInt(value)

        super(NodChannelRotateOrder, self).set(value)

    def _toInt(self, value):

        return self.STRING_TO_INT.get(value, ValueError('Invalid rotate order value: "{}"'.format(value)))


